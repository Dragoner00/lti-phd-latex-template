# LTI PHD Latex Template

Unoffical repository providing an LaTex template which matches most of the requirements to publish in [Spektrum der Lichttechnik](https://www.ksp.kit.edu/index.php?link=series_title&seriesID=67) of the publisher "Karlsruhe Institute of Technology Scientific Publishing". If you write a phd thesis at the [Lichttechnisches Institut](https://www.lti.kit.edu/index.php), you should take a look! The template may not be perfect, but it gives you a good start. Improvements are appreciated.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
