% ATTENTAION: Not everything is defined here. Some setting you may find in phd_main.tex.

%% Vorlage für LTI
%% --
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{LTI_main}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Schriftart Palatino
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage{mathpazo}
\usepackage[scaled=.95]{helvet}
\usepackage{courier}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Klasse book
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\LoadClass[a5paper,10pt,twoside,openright,titlepage,DIV=calc]{scrbook} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Verwendete Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[a5paper,includeheadfoot]{geometry}			% Seitenränder einstellen 
\usepackage[ngerman]{babel} 	 					% „Inhaltsverzeichnis“ statt „table of contents“ usw.
%\usepackage[ngerman=ngerman-x-latest]{hyphsubst} 	% Richtiges Trennen
\usepackage{hyphsubst}
\usepackage[utf8]{inputenc}							% Eingabe nach latin1 für Windows
\usepackage{graphicx} 
\usepackage{makeidx}          						% wir wollen auch einen Index
\usepackage[clearempty]{titlesec} 					% Textüberschriften anpassen
\usepackage{amssymb,amsmath}						% Mathematische Formatierung

\usepackage{paralist} 								% Compacte Listen
%\usepackage[section]{placeins} 					% verhindert Floats hinter dem Befehl \FloatBarrier
\usepackage{wrapfig} 								% ermöglicht Schrift umflossene Bilder und Tabellen
% \usepackage[innercaption]{sidecap} 				% Bildunterschrift auch neben dem Bild möglich
\usepackage{tabularx} 								% automatische Spaltenbreite von Tabellen
\usepackage{hhline} 								% Vertical lines crossing double horizontal lines in table
\usepackage{rotating} 								% zum drehen von bildern 
\usepackage{fancyhdr}								% definiere einfache Headings 
\usepackage{caption}    							% ATTENTION: Setting parameters below!
\usepackage{ifthen} 								% Bedingte Abfragen ermöglichen
\usepackage{epstopdf}
\usepackage[table]{xcolor}
\usepackage{microtype}
\usepackage[hang]{footmisc}							% Format footnotes
% \usepackage{pxfonts} 								% Do not use it, it desturbes math enviroments
\usepackage[breaklinks,
						raiselinks=true,%
            bookmarks=true,%
            bookmarksopenlevel=1,%
            bookmarksopen=true,%
            bookmarksnumbered=true,%
            hyperindex=true,% 
            plainpages=false,				% correct hyperlinks
            pdfpagelabels=true,				% view TeX pagenumber in PDF reader
            pdfborder={0 0 0.5}]{hyperref} 	% erzeuge Hyperlinks z.B. für pdflatex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Special Changes start. You may not need it
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[separate-uncertainty=true]{siunitx}
\sisetup{output-decimal-marker = {,}}

\usepackage{upgreek}		%\upvarepsilon
\usepackage{mathtools}		% matrix* (see the star?)
\usepackage{xfrac}			%\sfrac
\usepackage{multirow}
\usepackage{subcaption}		% \subfigure
\usepackage{listings}
\usepackage{mdframed}
\usepackage{acronym}
\usepackage{enumitem}

\DeclareMathOperator{\sgn}{sgn}

\renewcommand{\lstlistingname}{Programmauflistung}

\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}
\definecolor{Darkgreen}{rgb}{0,0.4,0}
\definecolor{grey}{rgb}{0.9,0.9,0.9}
\definecolor{lstgreen}{rgb}{0,0.5,0}
\lstset{
    backgroundcolor=\color{lbcolor},
    tabsize=4,
  language=C++,
  %captionpos=a,
  tabsize=4,
  literate=*{*}{\normalfont{*}}1,
  frame=lines,
  numbers=left,
  numberstyle=\scriptsize,
  numbersep=5pt,
  breaklines=true,
  morekeywords={__m256d},
  showstringspaces=false,
  basicstyle=\footnotesize,
  %identifierstyle=\color{magenta},
  keywordstyle=\color[rgb]{0,0,1},
  commentstyle=\color{Darkgreen},
  stringstyle=\color{red},
  emph={VectorN, Matrix},
  emphstyle={\color{Darkgreen}},
  xleftmargin=1.5em,
  %framexleftmargin=1.5em
  }

\newcommand{\tleft}{\text{left}}
\newcommand{\tright}{\text{right}}
\newcommand{\tndu}{\text{ndu}}
\newcommand{\tsaved}{\text{saved}}
\newcommand{\ttemp}{\text{temp}}
\newcommand{\tders}{\text{ders}}
\newcommand{\trk}{\text{rk}}
\newcommand{\tpk}{\text{pk}}
\newcommand{\uAVX}{_\text{AVX}}

\usepackage[backend=bibtex,
style=numeric-comp,
sorting=none,
giveninits=true,
maxnames=5
]{biblatex}
\usepackage{bibentry}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Special Changes end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Format the table of content
\usepackage{tocloft}
\renewcommand{\cftchapfont}{\bfseries\rmfamily}
\renewcommand\cftchapdotsep{\cftdotsep}
\renewcommand{\cfttoctitlefont}{\fontsize{14}{14}\bfseries\scshape\raggedright}

%% Format list of figures
% Distance between text and page number
\setlength{\cftfignumwidth}{25pt}
% Title of list of figures
\renewcommand{\cftloftitlefont}{\fontsize{12}{12}\bfseries\scshape\raggedright}
\setlength{\cftbeforeloftitleskip}{-5pt}
% Additional horizontal spacing in list of figures before figure number is shown  
\setlength{\cftfigindent}{0em}

%% Format list of tables
% Distance between text and page number
\setlength{\cfttabnumwidth}{25pt}
% Title of list of tables
\renewcommand{\cftlottitlefont}{\fontsize{12}{12}\bfseries\scshape\raggedright}
\setlength{\cftbeforelottitleskip}{-5pt}
% Additional horizontal spacing in list of figures before figure number is shown  
\setlength{\cfttabindent}{0em}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PDF Einstellungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\pdfcompresslevel=9   		% scheint zwar nichts zu bringen, aber was soll's
%\pdfimageresolution=1200 	% Nicht vektorisierte Bilder mit 1200dpi einbinden

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Seitenformat / Layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\geometry{left=2cm,right=1.8cm,top=0cm,headheight=18mm, bottom=14.4mm}
\linespread{1.2}	% Zeilenabstand
\parindent 0cm 		% Absatzanfang nicht eingerücken    
\parskip 8pt		% Abstand zwischen zwei Absätzen               


\DeclareCaptionFont{eightpt}{\fontsize{8pt}{9pt}\selectfont #1}
% Captions for table and figures
\captionsetup[table]{justification=justified, singlelinecheck=false, position=above, skip=6pt, font=eightpt} % Captions above Table

\captionsetup{labelfont={bf},format=plain, skip=6pt, justification=justified, font=eightpt} % Formatierung für die Bildunterschrift und Tabellenüberschrift

\setlength{\emergencystretch}{1.2em}
\setlength\parskip{.5\baselineskip
	plus .1\baselineskip
	minus .4\baselineskip
}

%
%% Schusterjungen und Hurenkinder verhindern. Siehe deutsches TeX-FAQ (6.1.3)
\clubpenalty = 9999
\widowpenalty = 9999
%\displaywidowpenalty = 9999

%% Same spacing / no stretching around align enviroments 
% https://tex.stackexchange.com/questions/237762/remove-vertical-space-around-align-equations
\setlength{\abovedisplayskip}{6pt}
\setlength{\belowdisplayskip}{6pt}
\setlength{\abovedisplayshortskip}{6pt}
\setlength{\belowdisplayshortskip}{6pt}

%% Head and Foot Line
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhf{} % clear all header and footer fields
\fancyhead[CE]{\scshape\fontsize{10pt}{10pt}\selectfont\nouppercase{\leftmark}} % header for left side (odd)
\fancyhead[CO]{\scshape\fontsize{10pt}{10pt}{\rightmark}} % right header for even pages
\fancyfoot[C]{\rmfamily\fontsize{10pt}{10pt}\selectfont\thepage}
\renewcommand{\headrulewidth}{0.71pt}
\renewcommand{\footrulewidth}{0.71pt}


%% Kopf und Fußzeile bei Kapitelseiten, siehe http://x2on.de/2009/02/25/latex-fusszeile-auf-jeder-seite/
\fancypagestyle{plain}{%
   %\fancyhf{~} 		% clear all header and footer fields
   \fancyhead[C]{~} 	% Kapitelname ausblenden
   % \fancyfoot[C]{~}	% Seitenzahl ausblenden
   \renewcommand{\headrulewidth}{0pt}
}

%% Format Footnotes
\setlength{\footnotemargin}{1em}
\setlength{\skip\footins}{20pt}


%% Format of headlines before and after main part (e. g. for abstract, appendix...)
\newcommand{\UseSecondaryCapterFormat}{\titleformat{\chapter}[hang]
	{\fontsize{12pt}{12pt}\bfseries\scshape\raggedright}{\thechapter\quad}{0pt}{}
	\titlespacing{\chapter}{0pt}{-2.1mm}{30pt}}

% Chapter headlines are defined in phd_main.tex

% Section
\titleformat{\section}{\fontsize{12}{12}\bfseries\scshape\raggedright}{\thesection}{3.5mm}{}

% The publisher wants fixed spacing. Avoid stretching here 
% https://en.wikibooks.org/wiki/LaTeX/Lengths
% https://tex.stackexchange.com/questions/108684/spacing-before-and-after-section-titles
% https://tex.stackexchange.com/questions/64756/what-is-glue-stretching
\titlespacing*{\section} {0pt}{10mm}{5mm}

% Subsection
\titleformat{\subsection}{\fontsize{10}{10}\bfseries\scshape\raggedright}{\thesubsection}{2mm}{}
\titlespacing*{\subsection} {0pt}{10mm}{5mm}

%% Format Bibliography
\renewcommand*{\bibfont}{\raggedright}
%\setcounter{biburlnumpenalty}{100}
%\setcounter{biburllcpenalty}{100}
%\setcounter{biburlucpenalty}{100}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stil Index-Seite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\renewenvironment{theindex}
%{\if@twocolumn
%   \@restonecolfalse
% \else
%   \@restonecoltrue
% \fi
% \columnseprule \z@
% \columnsep 35\p@
% \twocolumn[\@makeschapterhead{\indexname}]%
% \@mkboth{\indexname}%
%         {\indexname}%
% \addcontentsline{toc}{chapter}{\indexname}%
% \thispagestyle{fancy}
% \flushbottom
% \parindent\z@
% \parskip\z@ \@plus .3\p@\relax
% \let\item\@idxitem
% \def\,{\relax\ifmmode\mskip\thinmuskip
%              \else\hskip0.2em\ignorespaces\fi}%
% \raggedright}
%
%\newtheorem{definition}{Definition}[chapter]
%\newtheorem{satz}{Satz}[chapter]

%% **** END OF CLASS ****
